package TestTicketautomat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Ticket {
    private int id;
    private LocalDateTime outputTimestamp = LocalDateTime.now();
    private LocalDateTime paidTimestamp = LocalDateTime.now();


    public Ticket(int id) {
        this.id = id;

    }

    public int getId() {
        return id;
    }

    public LocalDateTime getOutputTimestamp() {
        return outputTimestamp;
    }

    public LocalDateTime getPaidTimestamp() {
        return paidTimestamp;
    }
}
