package TestTicketautomat;

public class Output {
    private boolean hasOutput;

    public Output(boolean hasOutput) {
        this.hasOutput = hasOutput;
    }

    public boolean isHasOutput() {
        return hasOutput;
    }
}
