package TestTicketautomat;


import java.time.LocalDateTime;
import java.util.ArrayList;

public class Controller {
    private String color;
    private Printer printer;
    private ArrayList<Ticket> allTickets = new ArrayList<>();

    private int price = 10;


    public Controller(String color) {
        this.color = color;
    }


    public ArrayList<Ticket> getAllTickets() {
        return allTickets;
    }

    public void getPrice(int id) {
        int amount = 0;
        if (allTickets.size() == 0) {
            System.out.println("There are no tickets!");
        } else {
            amount += price * allTickets.get(0).getOutputTimestamp().getSecond() - allTickets.get(0).getPaidTimestamp().getSecond();
        }
    }
}
