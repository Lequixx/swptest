package TestTicketautomat;

import javax.lang.model.type.NullType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

public class Ticketmachine {
    private Ticket ticket;
    private Controller controller;
    private Printer printer;
    private Output output;
    private Ticketscanner scanner;
    private Random random;
    private double balance = 0;


    public Ticketmachine(Controller controller, Printer printer, Output output, Ticketscanner scanner) {
        this.controller = controller;
        this.printer = printer;
        this.output = output;
        this.scanner = scanner;
    }

    public void getTicket() {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        Random random = new Random();
        Ticket ticket = new Ticket(random.nextInt(1000));
        this.controller.getAllTickets().add(ticket);
        System.out.println("Here is your ticket! " + "ID: " + ticket.getId() + " Outputstamp: " + ticket.getOutputTimestamp().format(myFormatObj));
    }

    public void currentTickets() {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        System.out.println("Current Tickets: ");
        LocalDateTime paid = null;
        for (int i = 0; i < controller.getAllTickets().size(); i++) {
            System.out.println("ID: " + controller.getAllTickets().get(i).getId() + " Outputstamp: " + controller.getAllTickets().get(i).getOutputTimestamp().format(myFormatObj)+ " Paid: " + paid);
        }
    }

    public void insertTicket(int id) {
        if (controller.getAllTickets().contains(id)) {
            System.out.println("Ticket "+ ticket.getId() + " inserted!");
        } else {
            System.out.println("This ticked id is not available");
        }
    }

    public void insertMoney(double amount) {
        if (amount > 0) {
            balance = balance + amount;
            System.out.println("You paid " + amount + "$");
        } else {
            System.out.println("Use a positive amount rather than: " +
                    amount);
        }

    }
}
