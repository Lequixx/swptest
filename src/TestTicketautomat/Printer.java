package TestTicketautomat;

import java.util.ArrayList;

public class Printer {
    private String color;

    public Printer(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
