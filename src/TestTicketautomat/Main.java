package TestTicketautomat;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller("black");
        Printer printer = new Printer("green");
        Output output = new Output(true);
        Ticketscanner ticketscanner = new Ticketscanner("yellow", true);
        Ticketmachine ticketmachine = new Ticketmachine(controller, printer, output, ticketscanner);

        int input = 0;

        while (input != 5) {
            System.out.println("1. Get ticket");
            System.out.println("2. See current tickets");
            System.out.println("3. Insert ticket");
            System.out.println("4. Pay ticket");
            System.out.println("5. Exit");

            double amount = 0;
            int id = 0;
            Scanner in = new Scanner(System.in);
            input = in.nextInt();
            switch (input) {
                case 1:
                    ticketmachine.getTicket();
                    break;
                case 2:
                    ticketmachine.currentTickets();
                    break;
                case 3:
                    System.out.println("Give the ticket id you want to insert");
                    id = in.nextInt();
                    ticketmachine.insertTicket(id);
                    System.out.println("You have to pay: ");
                    controller.getPrice(id);
                    break;
                case 4:
                    controller.getPrice(id);
                    System.out.println("Paying: ");
                    amount = in.nextDouble();
                    ticketmachine.insertMoney(amount);
                    break;
                case 5:
                    System.out.println("Exit");
                    break;
                default:
                    System.out.println("You can only choose from option 1-5");
                    break;
            }
        }
    }
}