package TestTicketautomat;

public class Ticketscanner {
    private String color;
    private boolean hasScanned;

    public Ticketscanner(String color, boolean hasScanned) {
        this.color = color;
        this.hasScanned = hasScanned;
    }

    public String getColor() {
        return color;
    }

    public boolean isHasScanned() {
        return hasScanned;
    }
}
